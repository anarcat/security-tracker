CVE-2017-5753
	[stretch] - nvidia-graphics-drivers 384.111-4~deb9u1
CVE-2017-5754
	[stretch] - nvidia-graphics-drivers 384.111-4~deb9u1
CVE-2017-5715
	[stretch] - nvidia-graphics-drivers 384.111-4~deb9u1
CVE-2017-12424
	[stretch] - shadow 1:4.4-4.1+deb9u1
CVE-2015-XXXX [busybox: pointer misuse unziping files]
	[stretch] - busybox 1:1.22.0-19+deb9u1
	NOTE: For #803097
CVE-2016-2148
	[stretch] - busybox 1:1.22.0-19+deb9u1
CVE-2016-2147
	[stretch] - busybox 1:1.22.0-19+deb9u1
CVE-2011-5325
	[stretch] - busybox 1:1.22.0-19+deb9u1
CVE-2017-15873
	[stretch] - busybox 1:1.22.0-19+deb9u1
CVE-2017-16544
	[stretch] - busybox 1:1.22.0-19+deb9u1
CVE-2017-17840
	[stretch] - open-iscsi 2.0.874-3~deb9u2
CVE-2017-7458
	[stretch] - ntopng 2.4+dfsg1-3+deb9u1
CVE-2017-7459
	[stretch] - ntopng 2.4+dfsg1-3+deb9u1
CVE-2017-17440
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-15266
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-15267
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-15600
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-15601
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-15602
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-15922
	[stretch] - libextractor 1:1.3-4+deb9u1
CVE-2017-1000494
	[stretch] - miniupnpd 1.8.20140523-4.1+deb9u1
CVE-2017-15105
	[stretch] - unbound 1.6.0-3+deb9u2
CVE-2017-16612
	[stretch] - wayland 1.12.0-1+deb9u1
CVE-2017-14804
	[stretch] - obs-build 20160921-1+deb9u1
CVE-2018-7667
	[stretch] - adminer 4.2.5-3+deb9u1
CVE-2018-1000159
	[stretch] - tlslite-ng 0.6.0-1+deb9u1
CVE-2018-1000156
	[stretch] - patch 2.7.5-1+deb9u1
CVE-2017-8109
	[stretch] - salt 2016.11.2+ds-1+deb9u2
CVE-2018-1059
	[stretch] - dpdk 16.11.6-1+deb9u1
CVE-2017-12627
	[stretch] - xerces-c 3.1.4+debian-2+deb9u1
CVE-2016-10317
	[stretch] - ghostscript 9.20~dfsg-3.2+deb9u2
CVE-2018-10194
	[stretch] - ghostscript 9.20~dfsg-3.2+deb9u2
CVE-2017-9218
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9219
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9220
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9221
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9222
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9223
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9253
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9254
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9255
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9256
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2017-9257
	[stretch] - faad2 2.8.0~cvs20161113-1+deb9u1
CVE-2018-10017
	[stretch] - libopenmpt 0.2.7386~beta20.3-3+deb9u3
CVE-2018-1302
	[stretch] - apache2 2.4.25-3+deb9u5
CVE-2018-10689
	[stretch] - blktrace 1.1.0-2+deb9u1
CVE-2018-11410
	[stretch] - liblouis 3.0.0-3+deb9u2
CVE-2017-5715
	[stretch] - intel-microcode 3.20180425.1~deb9u1
